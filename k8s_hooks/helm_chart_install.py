"""Execute the AWS CLI update-kubeconfig command."""

import os
import subprocess
import logging
import time
import yaml

import stacker
from distutils.version import LooseVersion
LEGACY_STACKER = LooseVersion(stacker.__version__) < LooseVersion('1.6.0')
if LEGACY_STACKER:
    from stacker.lookups.handlers.output import handler as output_handler  # noqa
else:
    from stacker.lookups.handlers.output import OutputLookup  # noqa pylint: disable=no-name-in-module,line-too-long
from stacker.session_cache import get_session

from stacker.variables import Variable, resolve_variables

if not LEGACY_STACKER:
    output_handler = OutputLookup.handle  # noqa

LOGGER = logging.getLogger(__name__)
dir_path = os.path.dirname(os.path.realpath(__file__))


def get_variables(variables, provider, context):
    converted_variables = [
        Variable(k, v) for k, v in variables.items()
    ]
    print(converted_variables)
    resolve_variables(
        converted_variables, context, provider
    )    
    return {v.name: v.value for v in converted_variables}


def helm_rbac():
    command = "kubectl apply -f " + dir_path + "/tiller.yaml"
    subprocess.check_output(command, shell=True)
    return True

def helm_install(provider, context, **kwargs):  # noqa pylint: disable=unused-argument
    """Init and install helm charts.
    Args:
        provider (:class:`stacker.providers.base.BaseProvider`): provider
            instance
        context (:class:`stacker.context.Context`): context instance

    Returns: boolean for whether or not the hook succeeded.
    """
    variables = get_variables(kwargs, provider, context)

    










    """
    albingressrole = output_handler(kwargs.get('albingressrole'),
                                  provider=provider,
                                  context=context)
    print(albingressrole)
    eksclusterautoscalerole = output_handler(kwargs.get('eksclusterautoscalerole'),
                                  provider=provider,
                                  context=context)
    cmd = "helm init --service-account tiller"
    helmrbac=helm_rbac()
    LOGGER.info('installing helm')
    env_vars = os.environ
    subprocess.call(cmd, shell=True, env=env_vars)
    LOGGER.info('helm and tiller successfully configured...')
    time.sleep(20)
    
    subprocess.call("helm repo update", shell=True, env=env_vars)
    subprocess.call("helm registry install quay.io/coreos/alb-ingress-controller-helm --namespace=kube-devops --name=alb-ingress-controller -f ./helm-charts/alb-ingress-controller/values.yaml --set clusterName=%s,podAnnotations.\"iam\.amazonaws\.com/role\"=%s" % (clustername,albingressrole), shell=True, env=env_vars)
    subprocess.call("helm repo add gitlab https://charts.gitlab.io",shell=True, env=env_vars)
    charts = ['stable/cluster-autoscaler']
    if helmrbac:
        for chart in charts:
            name = chart.split('/')[-1]
            command = "helm install --name %s --namespace kube-devops %s -f ./helm-charts/%s/values.yaml --set autoDiscovery.clusterName=%s,podAnnotations.\"iam\.amazonaws\.com/role\"=%s" % (name,chart,name,clustername,eksclusterautoscalerole)
            print("Running command " + command)
            subprocess.call(command, shell=True, env=env_vars)
    subprocess.call("helm install --name metrics-server --namespace kube-system stable/metrics-server", shell=True, env=env_vars)
    subprocess.call("kubectl annotate namespace kube-devops iam.amazonaws.com/permitted='.*'", shell=True, env=env_vars)
    subprocess.call("kubectl annotate namespace kube-system iam.amazonaws.com/permitted='.*'", shell=True, env=env_vars)
    """
    return True

